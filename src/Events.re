open Webapi.Dom;

let rec onFrame =
        (~stateRef: ref(Types.state), lastTime: float, newTime: float): unit => {
  let action: Types.action = Tick((newTime -. lastTime) /. 1000.);
  stateRef := Reducer.apply(~state=stateRef^, ~action);
  Webapi.requestAnimationFrame(onFrame(~stateRef, newTime));
  ();
};

let onKeyDown = (~stateRef: ref(Types.state), event: Dom.keyboardEvent) => {
  let action: Types.action = KeyDown(KeyboardEvent.code(event));
  stateRef := Reducer.apply(~state=stateRef^, ~action);
};

let onKeyUp = (~stateRef: ref(Types.state), event: Dom.keyboardEvent) => {
  let action: Types.action = KeyUp(KeyboardEvent.code(event));
  stateRef := Reducer.apply(~state=stateRef^, ~action);
};

let onMouseMove = (~stateRef: ref(Types.state), event: Dom.mouseEvent) => {
  let action: Types.action =
    MouseMove(MouseEvent.clientX(event), MouseEvent.clientY(event));
  stateRef := Reducer.apply(~state=stateRef^, ~action);
};

let onMouseDown = (~stateRef: ref(Types.state), _: Dom.mouseEvent) => {
  let action: Types.action = MouseDown;
  stateRef := Reducer.apply(~state=stateRef^, ~action);
};

let onMouseUp = (~stateRef: ref(Types.state), event: Dom.mouseEvent) => {
  let action: Types.action =
    MouseUp(MouseEvent.clientX(event), MouseEvent.clientY(event));
  stateRef := Reducer.apply(~state=stateRef^, ~action);
};
