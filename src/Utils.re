let setCanvasFullScreen: Types.canvas => unit = [%bs.raw
  {| function (canvas) {
    canvas.width = innerWidth;
    canvas.height = innerHeight;
  }|}
];

[@bs.get] external getWidth: Types.canvas => float = "width";

[@bs.get] external getHeight: Types.canvas => float = "height";

[@bs.val] external pi: float = "Math.PI";

[@bs.send]
external drawCanvas:
  (Types.canvasContext, ~canvas: Types.canvas, ~x: float, ~y: float) => unit =
  "drawImage";

let lerp = (start: float, stop: float, k: float): float => {
  start +. (stop -. start) *. k;
};

let loopIndex = (index: int, ~length: int) => {
  (length + index mod length) mod length;
};

let loopFloat = (n: float, ~max: float) => {
  mod_float(max +. mod_float(n, max), max);
};

let dist2dSq = (~x1: float, ~y1: float, ~x2: float, ~y2: float) => {
  let distX = x2 -. x1;
  let distY = y2 -. y1;
  distX *. distX +. distY *. distY;
};

let doesCirclesCollides = (c1: Types.transform, c2: Types.transform): bool => {
  let distX = c2.x -. c1.x;
  let distY = c2.y -. c1.y;
  let distR = c2.r -. c1.r;
  distX *. distX +. distY *. distY < distR *. distR;
};
