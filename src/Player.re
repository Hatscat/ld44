let initialPlayer = (): Types.player => {
  let (x, y) = Map.xyFromCellIndex(Constants.playerStartCell);
  {
    transform: {
      x,
      y,
      r: Constants.playerRadiusMin,
      cell: Constants.playerStartCell,
    },
    life: Constants.playerLifeMin,
    speedLvl: 0,
    rangeLvl: 0,
    damageLvl: 0,
    fireRateLvl: 0,
    aimDir: 0.,
    moveDir: {
      left: false,
      right: false,
      up: false,
      down: false,
    },
  };
};

let getSpeed = (player: Types.player): float => {
  Utils.lerp(
    Constants.playerSpeedMin,
    Constants.playerSpeedMax,
    float(player.speedLvl) /. float(Constants.upgradeMax),
  );
};
let getRange = (player: Types.player): float => {
  Utils.lerp(
    Constants.playerRangeMin,
    Constants.playerRangeMax,
    float(player.rangeLvl) /. float(Constants.upgradeMax),
  );
};
let getDamage = (player: Types.player): float => {
  Utils.lerp(
    Constants.playerDamageMin,
    Constants.playerDamageMax,
    float(player.damageLvl) /. float(Constants.upgradeMax),
  );
};
let getFireRate = (player: Types.player): float => {
  Utils.lerp(
    Constants.playerFireRateMin,
    Constants.playerFireRateMax,
    float(player.fireRateLvl) /. float(Constants.upgradeMax),
  );
};

let move = (state: Types.state, ~deltaTime: float): Types.state => {
  let maybeDir =
    switch (state.player.moveDir) {
    | {left: true, right: true} => None
    | {up: true, down: true} => None
    | {left: true, up: true} => Some(Utils.pi *. 0.75)
    | {left: true, down: true} => Some(Utils.pi *. 1.25)
    | {left: true} => Some(Utils.pi)
    | {right: true, up: true} => Some(Utils.pi *. 0.25)
    | {right: true, down: true} => Some(Utils.pi *. 1.75)
    | {right: true} => Some(0.)
    | {up: true} => Some(Utils.pi *. 0.5)
    | {down: true} => Some(Utils.pi *. 1.5)
    | _ => None
    };
  switch (maybeDir) {
  | Some(dir) =>
    let speed = getSpeed(state.player) *. deltaTime;
    let newX = Map.wrapX(state.player.transform.x +. cos(dir) *. speed);
    let newY = Map.wrapY(state.player.transform.y -. sin(dir) *. speed);
    let newCell = Map.cellIndexFromXy((newX, newY));
    if (state.map.cells[newCell].kind === Wall) {
      state;
    } else {
      {
        ...state,
        player: {
          ...state.player,
          transform: {
            r: state.player.transform.r,
            x: newX,
            y: newY,
            cell: newCell,
          },
        },
      };
    };
  | None => state
  };
};

let getMoveDir =
    (moveDir: Types.multipleDir, ~keyCode: Types.keyCode, ~isKeyDown: bool)
    : Types.multipleDir => {
  {
    left:
      switch (keyCode) {
      | "KeyA"
      | "ArrowLeft" => isKeyDown
      | _ => moveDir.left
      },
    right:
      switch (keyCode) {
      | "KeyD"
      | "ArrowRight" => isKeyDown
      | _ => moveDir.right
      },
    up:
      switch (keyCode) {
      | "KeyW"
      | "ArrowUp" => isKeyDown
      | _ => moveDir.up
      },
    down:
      switch (keyCode) {
      | "KeyS"
      | "ArrowDown" => isKeyDown
      | _ => moveDir.down
      },
  };
};
