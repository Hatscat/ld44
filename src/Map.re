let rawMap =
  {|
................................
.......................CX.......
....................XX.XC.......
.....................X.X........
...........XX.......XX..X.......
.............X......X...X.......
...X.....XXXXX..................
.X.X.......X............X.......
.X.XD..................XX.......
.X....C................X........
....W........R..................
................................
..C......C......................
...............XXXXXX.XXXXXX....
...............XF...............
.....XXXX.XXX..X.........CC.....
.........................CC.....
................................
|}
  |> Js.String.slice(~from=1, ~to_=-1)
  |> Js.String.split("\n");

let width: int = String.length(rawMap[0]);
let height: int = Array.length(rawMap);

Js.log(rawMap);
Js.log(width);
Js.log(height);

let cellFromString = (c: string): Types.cell => {
  let kind: Types.cellKind =
    switch (c) {
    | "X" => Wall
    | "C" => Chest
    | "D" => Shop(Damage)
    | "R" => Shop(Range)
    | "F" => Shop(FireRate)
    | "S" => Shop(Speed)
    | "W" => Spawn(Warrior)
    | "M" => Spawn(Mage)
    | "B" => Spawn(Boss)
    | _ => Empty
    };
  {kind, fog: Some(Constants.fogTimer)};
};

let cellIndexFromXy = (xy: (float, float)): int => {
  let x = int_of_float(fst(xy) /. Constants.cellSize);
  let y = int_of_float(snd(xy) /. Constants.cellSize);
  y * width + x;
};

let xyFromCellIndex = (cell: int): (float, float) => {
  let cellSize = int_of_float(Constants.cellSize);
  let x = cell mod width * cellSize + cellSize / 2;
  let y = cell / width * cellSize + cellSize / 2;
  (float(x), float(y));
};

let generateMap = (): Types.map => {
  {
    cells:
      rawMap
      |> Js.Array.reduce(
           (result: array(Types.cell), row: string) => {
             let cells =
               row |> Js.String.split("") |> Js.Array.map(cellFromString);
             Js.Array.concat(result, cells);
           },
           [||],
         ),
  };
};

let wrapX = (x: float): float => {
  Utils.loopFloat(x, ~max=float(width) *. Constants.cellSize);
};

let wrapY = (y: float): float => {
  Utils.loopFloat(y, ~max=float(height) *. Constants.cellSize);
};
