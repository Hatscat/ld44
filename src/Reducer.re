let apply = (~state: Types.state, ~action: Types.action): Types.state => {
  switch (state.stage) {
  | InGame => InGameReducer.apply(state, ~action)
  | _ => state
  };
};
