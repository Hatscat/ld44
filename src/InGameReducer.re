let handleKeyDown = (state: Types.state, ~keyCode: Types.keyCode): Types.state => {
  let moveDir =
    Player.getMoveDir(state.player.moveDir, ~keyCode, ~isKeyDown=true);
  {
    ...state,
    player: {
      ...state.player,
      moveDir,
    },
  };
};

let handleKeyUp = (state: Types.state, ~keyCode: Types.keyCode): Types.state => {
  let moveDir =
    Player.getMoveDir(state.player.moveDir, ~keyCode, ~isKeyDown=false);
  {
    ...state,
    player: {
      ...state.player,
      moveDir,
    },
  };
};

let handleMouseMove = (state: Types.state, ~x: int, ~y: int): Types.state => {
  {
    ...state,
    player: {
      ...state.player,
      aimDir:
        atan2(
          float(y) -. state.screenHeight /. 2.,
          float(x) -. state.screenWidth /. 2.,
        ),
    },
  };
};

let apply = (state: Types.state, ~action: Types.action): Types.state => {
  switch (action) {
  | Tick(deltaTime) =>
    state
    ->Player.move(~deltaTime)
    ->Fog.update(~deltaTime)
    ->InGameRender.drawStage
  | KeyDown(keyCode) => handleKeyDown(state, ~keyCode)
  | KeyUp(keyCode) => handleKeyUp(state, ~keyCode)
  | MouseMove(x, y) => handleMouseMove(state, ~x, ~y)
  | _ => state
  };
};
