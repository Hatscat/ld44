let minDistSq =
    (~playerTransform: Types.transform, ~cellX: float, ~cellY: float): float => {
  let distA =
    Utils.dist2dSq(
      ~x1=playerTransform.x,
      ~y1=playerTransform.y,
      ~x2=cellX,
      ~y2=cellY,
    );
  let distB =
    Utils.dist2dSq(
      ~x1=playerTransform.x,
      ~y1=playerTransform.y,
      ~x2=cellX -. float(Map.width) *. Constants.cellSize,
      ~y2=cellY,
    );
  let distC =
    Utils.dist2dSq(
      ~x1=playerTransform.x,
      ~y1=playerTransform.y,
      ~x2=cellX,
      ~y2=cellY -. float(Map.height) *. Constants.cellSize,
    );
  let distD =
    Utils.dist2dSq(
      ~x1=playerTransform.x,
      ~y1=playerTransform.y,
      ~x2=cellX -. float(Map.width) *. Constants.cellSize,
      ~y2=cellY -. float(Map.height) *. Constants.cellSize,
    );
  min(distA, min(distB, min(distC, distD)));
};

let update = (state: Types.state, ~deltaTime: float): Types.state => {
  let h = int_of_float(state.screenHeight /. Constants.cellSize);
  let w = int_of_float(state.screenWidth /. Constants.cellSize);

  //TODO: some duplicate code to merge with inGameRender.drawMap
  for (row in 0 to h) {
    let cellRow =
      Utils.loopIndex(
        state.player.transform.cell / Map.width - h / 2 + row,
        ~length=Map.height,
      );

    for (col in 0 to w) {
      let cellCol =
        Utils.loopIndex(
          state.player.transform.cell mod Map.width - w / 2 + col,
          ~length=Map.width,
        );
      let cellIndex = cellRow * Map.width + cellCol;
      let (cellX, cellY) = Map.xyFromCellIndex(cellIndex);
      let distSq =
        minDistSq(~playerTransform=state.player.transform, ~cellX, ~cellY);
      let distRatio = distSq /. Constants.fogDistanceSq;

      if (distRatio < 1.) {
        let fog =
          switch (state.map.cells[cellIndex].fog) {
          | Some(time) =>
            time < 0. ? None : Some(time -. deltaTime /. distRatio)
          | None => None
          };
        state.map.cells[cellIndex] = {
          kind: state.map.cells[cellIndex].kind,
          fog,
        };
        ();
      };
    };
  };
  state;
};
