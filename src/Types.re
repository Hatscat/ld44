type canvasContext = Webapi.Canvas.Canvas2d.t;

type canvas = Dom.element;

type keyCode = string; // TODO: to improve with variant

type action =
  | Empty
  | Tick(float)
  | KeyDown(keyCode)
  | KeyUp(keyCode)
  | MouseMove(int, int)
  | MouseDown
  | MouseUp(int, int);

type stage =
  | Menu
  | InGame
  | Pause;

type multipleDir = {
  left: bool,
  right: bool,
  up: bool,
  down: bool,
};

type transform = {
  x: float,
  y: float,
  r: float,
  cell: int,
};

type player = {
  transform,
  life: int,
  speedLvl: int,
  rangeLvl: int,
  damageLvl: int,
  fireRateLvl: int,
  aimDir: float,
  moveDir: multipleDir,
};

type enemyKind =
  | Warrior
  | Mage
  | Boss;

type upgradeKind =
  | Damage
  | Range
  | FireRate
  | Speed;

type cellKind =
  | Empty
  | Wall
  | Chest
  | Spawn(enemyKind)
  | Shop(upgradeKind);

type cell = {
  kind: cellKind,
  fog: option(float),
};

type map = {cells: array(cell)};

type state = {
  screenWidth: float,
  screenHeight: float,
  stage,
  player,
  map,
  canvasCtx: canvasContext,
  buffer: canvas,
  bufferCtx: canvasContext,
  noise: SimplexNoise.t,
};
