open Webapi;

let startGame = (canvas: Types.canvas, buffer: Types.canvas) => {
  Utils.setCanvasFullScreen(canvas);
  Utils.setCanvasFullScreen(buffer);

  let canvasCtx = Canvas.CanvasElement.getContext2d(canvas);
  let bufferCtx = Canvas.CanvasElement.getContext2d(buffer);

  // let noise: SimplexNoise.t = SimplexNoise.makeWithSeed("azerty"); // TODO: to remove
  // Js.log(noise##noise2D(0.5, 0.5));

  let stateRef =
    ref(
      Store.initialState(
        ~screenWidth=Utils.getWidth(canvas),
        ~screenHeight=Utils.getHeight(canvas),
        ~canvasCtx,
        ~buffer,
        ~bufferCtx,
        ~noise=SimplexNoise.make(),
      ),
    );

  Events.onFrame(~stateRef, 0., 0.);

  Dom.Document.addKeyDownEventListener(
    Events.onKeyDown(~stateRef),
    Dom.document,
  );
  Dom.Document.addKeyUpEventListener(
    Events.onKeyUp(~stateRef),
    Dom.document,
  );
  Dom.Document.addMouseMoveEventListener(
    Events.onMouseMove(~stateRef),
    Dom.document,
  );
  Dom.Document.addMouseDownEventListener(
    Events.onMouseDown(~stateRef),
    Dom.document,
  );
  Dom.Document.addMouseUpEventListener(
    Events.onMouseUp(~stateRef),
    Dom.document,
  );
};

switch (Dom.Document.getElementById("canvas", Dom.document)) {
| Some(canvas) =>
  startGame(canvas, Dom.Document.createElement("canvas", Dom.document));
  ();
| None => failwith("Error: canvas not found !")
};
