let playerLifeMin = 100;
let playerLifeMax = 10000;

let playerRadiusMin = 12.;
let playerRadiusMax = 32.;

let upgradeMax = 25;
let upgradeCost = 100;

let playerSpeedMin = 350.;
let playerSpeedMax = 1100.;

let playerRangeMin = 200.;
let playerRangeMax = 800.;

let playerDamageMin = 1.;
let playerDamageMax = 20.;

let playerFireRateMin = 10.;
let playerFireRateMax = 100.;

let cellSize = 128.;
let fogRadius = 90.;
let fogDistanceSq = 100000.;
let fogTimer = 0.7;
let playerStartCell = 1;

let hudHeight = 64.;

let lifeIcon = {js|❤️ |js};
let damageIcon = {js|💪 |js};
let rangeIcon = {js|🎯 |js};
let speedIcon = {js|👟 |js};
let fireRateIcon = {js|⚡️ |js};

let groundNoiseRatio = 0.03;
