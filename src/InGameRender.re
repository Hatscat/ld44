open Webapi.Canvas.Canvas2d;

let drawGroundCell =
    (
      ~x: float,
      ~y: float,
      ~ctx: Types.canvasContext,
      ~noise: SimplexNoise.t,
      ~cellX: int,
      ~cellY: int,
    )
    : unit => {
  let light =
    60.
    +. noise##noise2D(
         float(cellX) *. Constants.groundNoiseRatio,
         float(cellY) *. Constants.groundNoiseRatio,
       )
    *. 25.;
  setFillStyle(ctx, String, {j|hsl(150,30%,$light%)|j});
  ctx |> fillRect(~x, ~y, ~w=Constants.cellSize, ~h=Constants.cellSize);
};

let drawCellFog =
    (cell: Types.cell, ~x: float, ~y: float, ~ctx: Types.canvasContext) => {
  switch (cell.fog) {
  | Some(time) =>
    // let r = Constants.cellSize /. 2.;
    let alpha = time /. Constants.fogTimer;
    setFillStyle(ctx, String, {j|rgba(0,0,0,$alpha)|j});
    ctx
    |> fillRect(
         ~x=x -. 1.,
         ~y=y -. 1.,
         ~w=Constants.cellSize +. 2.,
         ~h=Constants.cellSize +. 2.,
       );
  // ctx |> beginPath;
  // ctx
  // |> arc(
  //      ~x=x +. r,
  //      ~y=y +. r,
  //      ~r=Constants.fogRadius,
  //      ~startAngle=0.,
  //      ~endAngle=Utils.pi *. 2.,
  //      ~anticw=false,
  //    );
  // ctx |> fill;
  | None => ()
  };
};

let drawMapCell =
    (
      cell: Types.cell,
      ~x: float,
      ~y: float,
      ~ctx: Types.canvasContext,
      ~noise: SimplexNoise.t,
      ~cellX: int,
      ~cellY: int,
    )
    : unit => {
  let (w, h) = (Constants.cellSize, Constants.cellSize);

  switch (cell.kind) {
  | Empty => drawGroundCell(~x, ~y, ~ctx, ~noise, ~cellX, ~cellY)
  | Wall =>
    setFillStyle(ctx, String, "#AAA");
    setStrokeStyle(ctx, String, "#777");
    ctx |> fillRect(~x, ~y, ~w, ~h);
    ctx |> strokeRect(~x, ~y, ~w, ~h);
  | Chest =>
    drawGroundCell(~x, ~y, ~ctx, ~noise, ~cellX, ~cellY);
    setFillStyle(ctx, String, "#FD0");
    let s = Constants.cellSize *. 0.2;
    ctx |> fillRect(~x=x +. s, ~y=y +. s, ~w=w -. s *. 2., ~h=h -. s *. 2.);
    setStrokeStyle(ctx, String, "#777");
    ctx |> strokeRect(~x=x +. s, ~y=y +. s, ~w=w -. s *. 2., ~h=h -. s *. 2.);
  | Spawn(enemyKind) =>
    setFillStyle(ctx, String, "#F00");
    ctx |> fillRect(~x, ~y, ~w, ~h);
  | Shop(upgradeKind) =>
    setFillStyle(ctx, String, "#00F");
    ctx |> fillRect(~x, ~y, ~w, ~h);
  };
  drawCellFog(cell, ~x, ~y, ~ctx);
};

let drawMap = (~ctx: Types.canvasContext, ~state: Types.state): unit => {
  let h = int_of_float(state.screenHeight /. Constants.cellSize);
  let w = int_of_float(state.screenWidth /. Constants.cellSize);

  for (row in (-1) to h + 1) {
    let cellY =
      Utils.loopIndex(
        state.player.transform.cell / Map.width - h / 2 + row,
        ~length=Map.height,
      );
    let y =
      state.screenHeight
      /. 2.
      -. float(h)
      /. 2.
      *. Constants.cellSize
      +. float(row)
      *. Constants.cellSize
      -. mod_float(state.player.transform.y, Constants.cellSize);

    for (col in (-1) to w + 1) {
      let cellX =
        Utils.loopIndex(
          state.player.transform.cell mod Map.width - w / 2 + col,
          ~length=Map.width,
        );
      let x =
        state.screenWidth
        /. 2.
        -. float(w)
        /. 2.
        *. Constants.cellSize
        +. float(col)
        *. Constants.cellSize
        -. mod_float(state.player.transform.x, Constants.cellSize);

      let cellIndex = cellY * Map.width + cellX;
      drawMapCell(
        state.map.cells[cellIndex],
        ~x,
        ~y,
        ~ctx,
        ~noise=state.noise,
        ~cellX,
        ~cellY,
      );
    };
  };
};

let drawPlayer = (~ctx: Types.canvasContext, ~state: Types.state): unit => {
  ctx->setFillStyle(String, "#FD0");
  ctx |> beginPath;
  ctx
  |> arc(
       ~x=state.screenWidth /. 2.,
       ~y=state.screenHeight /. 2.,
       ~r=
         Utils.lerp(
           Constants.playerRadiusMin,
           Constants.playerRadiusMax,
           float(state.player.life - Constants.playerLifeMin)
           /. float(Constants.playerLifeMax),
         ),
       ~startAngle=0.,
       ~endAngle=Utils.pi *. 2.,
       ~anticw=false,
     );
  ctx |> fill;
  setStrokeStyle(ctx, String, "#000");
  ctx |> stroke;
};

let drawAimRay = (~ctx: Types.canvasContext, ~state: Types.state): unit => {
  let range = state.player->Player.getRange;
  let (x, y) = (state.screenWidth /. 2., state.screenHeight /. 2.);
  ctx->setStrokeStyle(String, "#0F0");
  ctx |> beginPath;
  ctx |> moveTo(~x, ~y);
  ctx
  |> lineTo(
       ~x=x +. cos(state.player.aimDir) *. range,
       ~y=y +. sin(state.player.aimDir) *. range,
     );
  ctx |> stroke;
};

let drawHud = (~ctx: Types.canvasContext, ~state: Types.state): unit => {
  ctx->globalAlpha(0.3);
  ctx->setFillStyle(String, "#000");
  ctx |> fillRect(~x=0., ~y=0., ~w=state.screenWidth, ~h=Constants.hudHeight);
  ctx->globalAlpha(1.);
  ctx->setFillStyle(String, "#FFF");
  ctx->font("28px Verdana");
  ctx->textAlign("center");
  ctx->textBaseline("middle");
  let y = Constants.hudHeight *. 0.5;
  let x = n => state.screenWidth *. (n /. 6.);
  ctx
  |> fillText(
       Constants.lifeIcon ++ string_of_int(state.player.life),
       ~x=x(1.),
       ~y,
     );
  ();
  ctx
  |> fillText(
       Constants.speedIcon ++ string_of_int(state.player.speedLvl),
       ~x=x(2.),
       ~y,
     );
  ();
  ctx
  |> fillText(
       Constants.damageIcon ++ string_of_int(state.player.damageLvl),
       ~x=x(3.),
       ~y,
     );
  ();
  ctx
  |> fillText(
       Constants.fireRateIcon ++ string_of_int(state.player.fireRateLvl),
       ~x=x(4.),
       ~y,
     );
  ();
  ctx
  |> fillText(
       Constants.rangeIcon ++ string_of_int(state.player.rangeLvl),
       ~x=x(5.),
       ~y,
     );
  ();
};

let drawStage = (state: Types.state): Types.state => {
  let ctx = state.bufferCtx;
  drawMap(~ctx, ~state);
  drawAimRay(~ctx, ~state);
  drawPlayer(~ctx, ~state);
  drawHud(~ctx, ~state);
  state.canvasCtx->Utils.drawCanvas(~canvas=state.buffer, ~x=0., ~y=0.);
  state;
};
