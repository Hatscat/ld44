let initialState =
    (~screenWidth, ~screenHeight, ~canvasCtx, ~buffer, ~bufferCtx, ~noise)
    : Types.state => {
  screenWidth,
  screenHeight,
  stage: InGame,
  map: Map.generateMap(),
  player: Player.initialPlayer(),
  canvasCtx,
  buffer,
  bufferCtx,
  noise,
};
