type t = {
  .
  [@bs.meth] "noise2D": (float, float) => float,
  [@bs.meth] "noise3D": (float, float, float) => float,
  [@bs.meth] "noise4D": (float, float, float, float) => float,
};
type randomNumberGenerator = unit => float;

[@bs.new] [@bs.module] external make: unit => t = "simplex-noise";

[@bs.new] [@bs.module] external makeWithSeed: string => t = "simplex-noise";

[@bs.new] [@bs.module]
external makeWithGenerator: randomNumberGenerator => t = "simplex-noise";
