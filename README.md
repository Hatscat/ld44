# Ludum Dare 44 entry

My Ludum Dare 44 game jam entry.

## Development prerequisites

* [Node.js](https://nodejs.org)
* [Yarn](https://yarnpkg.com)

## Installation

From the project directory

```sh
# Install all dependencies
yarn
```

## Run the app on web browser

```sh
# build all reason source files in javascript (in /lib) + serve produced html, js and other resources on local server (localhost:1234)
yarn dev
```

Then open your browser at [`localhost:1234`](http://localhost:1234)

## Fix dev environment issues

* Fast command

```sh
# bsb -clean-world + rm .cache + rm dist + rm .merlin
yarn clean:all
```

* Complete command

```sh
# clean:all + rm lib + rm node_modules + yarn install
yarn rescue
```
